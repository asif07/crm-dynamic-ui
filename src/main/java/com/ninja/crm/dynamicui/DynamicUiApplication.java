package com.ninja.crm.dynamicui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com")
@EnableConfigurationProperties
@EntityScan(basePackages = {"com"})
public class DynamicUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DynamicUiApplication.class, args);
	}

}
