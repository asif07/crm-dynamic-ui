package com.ninja.crm.dynamicui.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "page_definition")
public class PageDefinition {

	@Id
	@GeneratedValue
	private Long pageId;
	private String pageName;

	@OneToMany(mappedBy = "pageDefinition")
	private List<ComponentDefinition> componentDefinition;

	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public List<ComponentDefinition> getComponentDefinition() {
		return componentDefinition;
	}

	public void setComponentDefinition(List<ComponentDefinition> componentDefinition) {
		this.componentDefinition = componentDefinition;
	}

	
}
