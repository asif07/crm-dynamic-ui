package com.ninja.crm.dynamicui.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="component_definition")
public class ComponentDefinition {
	
	@Id
	@GeneratedValue
	private Long componentId;
	private String type;
	private String label;
	private String uniqueId;
	private String dispParentYN;
	private String multiYN;
	private Integer seqNo;
	private String action;
	private String params;
	@Transient
	private List data = new ArrayList<>();
	@ManyToOne
    @JoinColumn(name="pageId", nullable=false)
	@JsonIgnore
    private PageDefinition pageDefinition;
	
	 @OneToMany
	    @JoinTable(name = "comp_def_values",
	            joinColumns = {@JoinColumn(name = "componentId")},
	            inverseJoinColumns = {@JoinColumn(name = "componentValueId")}
	    )
	 private List<ComponentValues> componentValues;

	public Long getComponentId() {
		return componentId;
	}

	public void setComponentId(Long componentId) {
		this.componentId = componentId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getDispParentYN() {
		return dispParentYN;
	}

	public void setDispParentYN(String dispParentYN) {
		this.dispParentYN = dispParentYN;
	}

	public String getMultiYN() {
		return multiYN;
	}

	public void setMultiYN(String multiYN) {
		this.multiYN = multiYN;
	}

	public Integer getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	public PageDefinition getPageDefinition() {
		return pageDefinition;
	}

	public void setPageDefinition(PageDefinition pageDefinition) {
		this.pageDefinition = pageDefinition;
	}

	public List<ComponentValues> getComponentValues() {
		return componentValues;
	}

	public void setComponentValues(List<ComponentValues> componentValues) {
		this.componentValues = componentValues;
	}

	public List getData() {
		return data;
	}

	public void setData(List data) {
		this.data = data;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}
	

}
