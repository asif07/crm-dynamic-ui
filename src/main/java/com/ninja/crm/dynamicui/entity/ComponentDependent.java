package com.ninja.crm.dynamicui.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "component_depnd")
public class ComponentDependent {

	@Id
	@GeneratedValue
	private Long compDpndId;
	@ManyToOne
	@JoinColumn(name = "componentValueId", nullable = false)
	private ComponentValues componentValues;

	
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "componentId")
    private ComponentDefinition componentDependent;

	public Long getCompDpndId() {
		return compDpndId;
	}

	public void setCompDpndId(Long compDpndId) {
		this.compDpndId = compDpndId;
	}


	public ComponentValues getComponentValues() {
		return componentValues;
	}

	public void setComponentValues(ComponentValues componentValues) {
		this.componentValues = componentValues;
	}

	public ComponentDefinition getComponentDependent() {
		return componentDependent;
	}

	public void setComponentDependent(ComponentDefinition componentDependent) {
		this.componentDependent = componentDependent;
	}
	

}
