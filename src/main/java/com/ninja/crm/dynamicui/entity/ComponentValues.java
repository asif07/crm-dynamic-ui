package com.ninja.crm.dynamicui.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="component_values")
public class ComponentValues {
	
	@Id
	@GeneratedValue
	private Long componentValueId;
	private String label;
	private String uniqueId;
	private String dispParentYN;
	private String multiYN;
	private Integer seqNo;
	private String action;
	private String params;
	@Transient
	private List data = new ArrayList<>();
	
	 @OneToMany
	    @JoinTable(name = "comp_values_def",
	            joinColumns = {@JoinColumn(name = "componentValueId")},
	            inverseJoinColumns = {@JoinColumn(name = "componentId")}
	    )
	 private List<ComponentDefinition> componentDefinition;

	public Long getComponentValueId() {
		return componentValueId;
	}

	public void setComponentValueId(Long componentValueId) {
		this.componentValueId = componentValueId;
	}


	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getDispParentYN() {
		return dispParentYN;
	}

	public void setDispParentYN(String dispParentYN) {
		this.dispParentYN = dispParentYN;
	}

	public String getMultiYN() {
		return multiYN;
	}

	public void setMultiYN(String multiYN) {
		this.multiYN = multiYN;
	}

	public Integer getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	public List<ComponentDefinition> getComponentDefinition() {
		return componentDefinition;
	}

	public void setComponentDefinition(List<ComponentDefinition> componentDefinition) {
		this.componentDefinition = componentDefinition;
	}

	public List getData() {
		return data;
	}

	public void setData(List data) {
		this.data = data;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	 
	 
}
