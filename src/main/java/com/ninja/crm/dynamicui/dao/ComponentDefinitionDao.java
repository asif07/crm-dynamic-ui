package com.ninja.crm.dynamicui.dao;

import com.ninja.crm.dynamicui.entity.PageDefinition;

public interface ComponentDefinitionDao {
	
	PageDefinition getPageDefinition(String pageName);

}
