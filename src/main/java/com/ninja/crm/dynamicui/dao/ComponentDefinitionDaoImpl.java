package com.ninja.crm.dynamicui.dao;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ninja.crm.dynamicui.entity.PageDefinition;

@Repository
public class ComponentDefinitionDaoImpl implements ComponentDefinitionDao{

	
	@Autowired
    private EntityManager entityManager;

    private Session getSession() {
        return entityManager.unwrap(Session.class);
    }


	
	@Override
	public PageDefinition getPageDefinition(String pageName) {
		try {
			Criteria criteria = getSession().createCriteria(PageDefinition.class)
        			.add(Restrictions.eq("pageName", pageName));
        	    return (PageDefinition) criteria.uniqueResult();
		} catch (Exception e) {
			return null;
		}
	}

}
