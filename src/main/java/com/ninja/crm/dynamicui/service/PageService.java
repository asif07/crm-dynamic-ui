package com.ninja.crm.dynamicui.service;

import com.ninja.crm.dynamicui.entity.PageDefinition;

public interface PageService {
	
	PageDefinition getComponentDefinition(String pageName);

}
