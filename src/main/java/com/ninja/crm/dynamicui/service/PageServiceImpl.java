package com.ninja.crm.dynamicui.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ninja.crm.dynamicui.dao.ComponentDefinitionDao;
import com.ninja.crm.dynamicui.entity.ComponentDefinition;
import com.ninja.crm.dynamicui.entity.ComponentValues;
import com.ninja.crm.dynamicui.entity.PageDefinition;

@Service
public class PageServiceImpl implements PageService {

	@Autowired
	private ComponentDefinitionDao componentDefinitionDao;

	@Override
	public PageDefinition getComponentDefinition(String pageName) {
		PageDefinition pageDefinition = componentDefinitionDao.getPageDefinition(pageName);

	// Setting Sample Data starts
		HashMap<String, String> myObjs = new HashMap<>();
		myObjs.put("customerName", "Test User");
		myObjs.put("language", "English");
		myObjs.put("facilityName", "Bellandur");
		myObjs.put("walletBalance", "300");
		myObjs.put("orderId", "1002");
		myObjs.put("orderMode", "Delivery");
		myObjs.put("orderValue", "1345");
		myObjs.put("orderFacility", "Bellandur");

		JSONArray tomorrowsOrders = new JSONArray();
		JSONObject jo = new JSONObject();
		jo.put("orderId", "1000");
		jo.put("value", "1345");
		jo.put("orderMode", "Delivery");
		jo.put("orderStatus", "Confirmed");
		jo.put("paymentMode", "Cash");
		jo.put("orderType", "Normal");
		jo.put("ticketsRaised", "0");
		tomorrowsOrders.add(jo);

		JSONArray todaysOrders = new JSONArray();
		JSONObject jo1 = new JSONObject();
		jo1.put("orderId", "1001");
		jo1.put("value", "1345");
		jo1.put("orderMode", "Delivery");
		jo1.put("orderStatus", "Dispatched");
		jo1.put("paymentMode", "Cash");
		jo1.put("orderType", "Normal");
		jo1.put("ticketsRaised", "0");
		todaysOrders.add(jo1);
		
		JSONArray completedOrders = new JSONArray();
		JSONObject completedObj = new JSONObject();
		completedObj.put("orderDate", "6-Jul");
		completedObj.put("orderId", "1002");
		completedObj.put("value", "1345");
		completedObj.put("orderMode", "Delivery");
		completedObj.put("orderStatus", "Fulfilled");
		completedObj.put("paymentMode", "Wallet");
		completedObj.put("orderType", "Normal");
		completedObj.put("ticketsRaised", "0");
		completedOrders.add(completedObj);
// Setting Sample Data Ends
		List<ComponentDefinition> components = pageDefinition.getComponentDefinition();
		List<ComponentDefinition> mainComponents = components.stream()
				.filter(component -> "Y".equals(component.getDispParentYN())).collect(Collectors.toList());
		pageDefinition.setComponentDefinition(mainComponents);

		for (ComponentDefinition def : pageDefinition.getComponentDefinition()) {
			if (def.getType().equalsIgnoreCase("tabs")) {
				for (ComponentValues values : def.getComponentValues()) {
					for (ComponentDefinition tabsComponents : values.getComponentDefinition()) {
						if (tabsComponents.getUniqueId().equalsIgnoreCase("tomorrows_orders")) {
							tabsComponents.setData(tomorrowsOrders);
						}
						if (tabsComponents.getUniqueId().equalsIgnoreCase("todays_orders")) {
							tabsComponents.setData(todaysOrders);
						}
						if (tabsComponents.getUniqueId().equalsIgnoreCase("completed_orders")) {
							tabsComponents.setData(completedOrders);
						}
					}
				}
			} 
			else {
				for (ComponentValues values : def.getComponentValues()) {
					if (myObjs.containsKey(values.getUniqueId())) {
						values.setData(Arrays.asList(myObjs.get(values.getUniqueId())));
					}
				}
			}
		}
		return pageDefinition;
	}

}
