package com.ninja.crm.dynamicui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ninja.crm.dynamicui.entity.PageDefinition;
import com.ninja.crm.dynamicui.service.PageService;

@Controller
public class TestController {
	
	@Autowired
	private PageService PageService;
	
	@GetMapping("/components")
	@ResponseBody
	public PageDefinition  getPageDefination(@RequestParam("pageName") String pageName) {
	    return PageService.getComponentDefinition(pageName);
	}
	
	@GetMapping("/componentByDynamicPageName")
	@ResponseBody
	public PageDefinition  componentByDynamicPageName(@RequestParam("ngModelName") String ngModelName) {
		String pageName = "";
		if("orderCancelled".equalsIgnoreCase(ngModelName)) {
			pageName = "OrderCancelledPage";
		}else if("orderDeleted".equalsIgnoreCase(ngModelName)) {
			pageName = "OrderDeletedPage";
		}else if("pickupSlot".equalsIgnoreCase(ngModelName)) {
			pageName = "PickupSlot";
		}else if("orderConfirmed".equalsIgnoreCase(ngModelName)) {
			pageName = "OrderConfirmed";
		}else if("editOrder".equalsIgnoreCase(ngModelName)) {
			pageName = "EditMyOrder";
		} else if("driverBehaviourIssue".equalsIgnoreCase(ngModelName)) {
			pageName = "DriverBehaviourIssue";
		}
		else {
			pageName = "Order";
		}
		
	    return PageService.getComponentDefinition(pageName);
	}
	
	@GetMapping("/dynamic-ui")
	public String index(Model model) {
	    return "index";
	}
}
